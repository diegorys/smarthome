install:
	sudo apt update
	sudo apt upgrade
	sudo apt install vim
	# OpenMediaVault pending
	sudo curl -fsSL get.docker.com | sed 's/buster/stretch/' | sh
	sudo usermod -aG docker pi
	sudo apt install docker-compose
portainer:
	cd services/portainer && docker-compose up -d
heimdall:
	cd services/heimdall && docker-compose up -d
netdata:
	cd services/netdata && docker-compose up -d
duplicati:
	cd services/duplicati && docker-compose up -d
plex:
	cd services/plex && docker-compose up -d
qbittorrent:
	cd services/qbittorrent && docker-compose up -d
calibre:
	cd services/calibre && docker-compose up -d
home-assistant:
	cd services/home-assistant && docker-compose up -d
all:
	install
	portainer
	heimdall
	netdata
	duplicati
	plex
	qbittorrent
	calibre
	home-assistant