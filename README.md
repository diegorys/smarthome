# SmartHome

Sistema SmartHome en una Raspberry Pi 3, con servicios dockerizados.

## Introducción y objetivos

Este proyecto nace de la necesidad de tener un servidor casero en local, capaz de gestionar las necesidades de un hogar digital. Los objetivos a cubrir son:
- Servidor físico en la red local.
- Instalación limpia de servicios en contenedores para hogar digital.

## Configuración básica del servidor

Para configurar el servidor, necesitamos:
- Montar físicamente la Raspberry Pi. Para este tutorial, se ha empleado una Raspberry Pi 3, con una tarjeta SD de 32 Gb, una carcasa sin ventilador ni disipador, y un cable de red para proporcionar acceso por red a la Raspberry Pi y dotarla de Internet. A la Raspberry Pi se le ha conectado un disco duro externo mediante USB.
- Instalar el sistema operativo:  En esta versión de SmartHome se ha instalado Raspbian GNU/Linux 10 (buster) en su versión Lite. Se emplea [Etcher](https://www.balena.io/etcher/) para su grabación en la tarjeta SD.
- Configurar acceso SSH: En la tarjeta SD, se debe crear un archivo vacío con el nombre "ssh" en la partición "/boot" Una vez configurado, metemos la tarjeta SD en la Raspberry Pi, la encendemos y nos conectamos a ella remotamente por ssh desde una terminal:
```
ssh pi@ipsmarthome
```
Donde "ipsmarthome" será la dirección IP que tu router le haya asignado a tu Raspberry Pi.

- Instalar git:
```
sudo apt install git
```
- Instalar el proyecto “SmartHome”: Se clona el repositorio del proyecto SmartHome:
```
git clone https://gitlab.com/diegorys/smarthome
```
Opcionalmente, puedes hacer un "fork" del proyecto para editar la configuración a tu gusto y mantenerla actualizada con tus datos.

Este repositorio contiene un archivo Makefile, capaz de realizar la tarea de instalación inicial, ejecutando “make install”.

Esta ejecución realiza los siguientes pasos:
1.  Actualización del sistema operativo: para obtener siempre las últimas versiones de cualquier aplicación que se instale, es altamente recomendable actualizar todos los paquetes disponibles:
```
sudo apt update
sudo apt upgrade
```
2. Instalación de vim.
```
sudo apt install vim
```
3. Instalación de OpenMediaVault.
```
sudo su
wget -O - https://github.com/OpenMediaVault-Plugin-Developers/installScript/raw/master/install | sudo bash
```
4. Instalación de Docker y Docker-Compose.
```
sudo curl -fsSL get.docker.com | sed 's/buster/stretch/' | sh
sudo usermod -aG docker pi
sudo apt install docker-compose
```

### Configurar fecha y hora

Desde la terminal, ejecutamos la siguiente orden:
```
sudo raspi-config
```
Desde la pantalla de configuración de la Raspbery Pi, puedes configurar la fecha y hora, localización, distribución de teclado y todo lo que necesites.

### Configuración de red

Para acceder siempre a la Raspberry Pi por una misma ip, la fijamos. Nos conectamos por ssh a la Raspberry Pi y, en la terminal, ejecutamos la siguiente orden:
```
sudo vim /etc/dhcpcd.conf
```

Añadimos las siguientes líneas:
```
interface wlan0
static ip_address=192.168.1.10/24
static routers=192.168.1.1
```
Donde ip_address se corresponde con la IP que quieras asignar a tu Raspberry Pi, y routers es la dirección IP de tu router, que hará de puerta de enlace.

Una vez tenemos la IP fija, en este proyecto nos referiremos a dicha IP como “ipsmarthome”, de modo que, cada vez que aparezca “ipsmarthome”, se debe reemplazar por la IP aquí configurada. Por ejemplo, si la IP de tu Raspberry Pi es 192.168.1.10 y en el documento se indica que se puede acceder a todos sus servicios desde http://ipsmarthome, significa que debes poner en la barra del navegador http://192.168.1.10.

### Usuario y contraseña

Se recomienda no utilizar el usuario (pi) y contraseña (raspberry) por defecto.

## Características de los servicios

- Se instalan siempre con Docker en contenedores.
- La configuración de los servicios debe ir siempre en /home/pi/config/xxxx (siendo xxxx un servicio), de modo que, a la hora de configurar un sistema de copias de seguridad, todas las configuraciones se encuentren localizadas en un único directorio a respaldar.
- Tus datos personales utilizados por los servicios deben almacenarse en el disco duro externo.
- Se recomienda mantener actualizados todos los servicios.
- Se recomienda monitorizar que todos los servicios estén activos.

## Instalación de servicios

En el repositorio clonado del proyecto **SmartHome**, se encuentran los instaladores de cada contenedores de cada servicio. Para instalar cada uno de ellos, simplemente se ejecuta:
```
make nombre_servicio
```
Se detalla en cada uno de los servicios.

## Servicios básicos

- [Portainer](./services/portainer/README.md)
- [Heimdall](./services/heimdall/README.md)
- [Netdata](./services/netdata/README.md)
- [Duplicati](./services/duplicati/README.md)

## Servidor Multimedia

- [Plex](./services/plex/README.md)
- [qBitTorrent](./services/qbittorrent/README.md)
- [Calibre](./services/calibre/README.md)

## Servidor Domótico

- [Home Assistant](./services/home-assistant/README.md)

## Referencias
- [Raspberry Pi 4 Home Server](https://www.youtube.com/playlist?list=PLhMI0SExGwfAU-UMeKxd1Lu5_a60AlA9N)
