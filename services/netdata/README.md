# Netdata

Servicio encargado de monitorizar el estado del sistema, tanto uso de RAM, como de procesador, de disco duro o de memoria SWAP.

1. En la terminal, ejecutar la orden:
```
make netdata
```
2. En tu navegador, accede a: http://ipsmarthome:19999.