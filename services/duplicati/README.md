# Duplicati
Servicio encargado de realizar copias de seguridad de la configuración de tus servicios y de tus datos.

## Instalación
En el archivo [docker-compose.yml](./docker-compose.yml) mapeamos los directorios de los que queremos hacer copias de seguridad, del siguiente modo:
- /srv/dev-disk-by-uuid-XXX/MisDatos1/:/MisDatos1
- /srv/dev-disk-by-uuid-XXX/MisDatos2/:/MisDatos2
- /srv/dev-disk-by-uuid-XXX/MisDatos2/:/MisDatos2
- /home/pi/config/:/RPIConfig

Donde los directorios “MisDatos” se ubican en tu disco duro y el directorio "RPIConfig" contiene toda la configuración de todos los servicios.

Ejecutamos en el terminal:

```
make duplicati
```

Accedemos a Duplicati: http://ipsmarthome:8200

Nos aparece un mensaje con título "First run steps", a que podemos decimos que sí después de leerlo.

## Preparación
Las copias se harán en la nube. Para este tutorial, necesitas una cuenta de Gmail donde guardar las copias de seguridad. Si no quieres usar Gmail, tienes muchas más opciones, elige la que más se adapte a tus necesidades.

## Crear la copia de seguridad de Config
Hacemos clic en "Copia de seguridad" y se nos abre la página de creación de una nueva copia.

### Configuración general:
- Nombre: Configuración.
- Descripción: Directorio donde se almacenan todos los archivos de configuración de todos los servicios instalados en este servidor.
- Cifrado: ninguno (pese a que no se recomiende, lo mantengo así, por si se olvida la contraseña de firaco, ya me fío yo de la seguridad de mi cuenta de Google Drive).

### Destino:
- Tipo de almacenamiento: Google Drive
- Ruta del servidor: Backups/RPISmartHome
- AuthID: Inicia sesión con tu cuenta de Google.

### Datos de origen:
- Computer / RPIConfig.
- Horario: Sólo cambiamos la hora: 4:00.

### Opciones generales:
- Mantener un número específico de copias de seguridad: 4. De este modo, todos los días se hará una copia de seguridad, y, al quinto día, reemplazará la más antigua, conservando siempre una copia de los últimos cuatro días.

## Crear las copias de seguridad de MisDatos
Siguiendo las mismas pautas, creamos tantas copias de seguridad de los directorios de “MisDatos”, con la periodicidad que necesitemos.

## Restaurar una copia
Explicación pendiente.

## Referencias
- [Raspberry Pi Home Server Episode 4: Server Backups - Duplicati and Google Drive](https://www.youtube.com/watch?v=-NyzdAYMarw)
- [Copias de Seguridad con Docker en una Raspberry Pi](https://www.zonagadget.com/2020/12/22/copias-de-seguridad-con-docker-en-una-raspberry-pi/)
