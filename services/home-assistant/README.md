# Home Assistant

Servicio encargado de integrar tus dispositivos domóticos para su monitorización y control.

## Instalación

1. En la terminal, ejecutar la orden:
```
make home-assistant
```
2. En tu navegador, accede a: http://ipsmarthome:8123.