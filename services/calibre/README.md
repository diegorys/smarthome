# Calibre

Servicio empleado para organizar tu colección de libros.

## Instalación

1. Reemplazar en el archivo [docker-compose.yml](./docker-compose.yml), en "volumes", "xxx" por el directorio donde vayas a guardar tu colección. El directorio no debe contener libros, ya los cargarás manualmente después, pero debes copiar dentro de ese directorio el archivo "metadata.db" para que funcione. De no hacerlo, más adelante te dará un error cuando pongas la ruta "/books" al configurar calibre.
2. En el terminal, ejecutar:
```
make calibre
```
3. Acceder a la web: http://ipsmarthome:8083
4. Entrar con el usuario y contraseña por defecto: admin/admin123
5. En la configuración, cambiar el idioma y habilitar "Subir archivos":
- admin -> Idioma -> Español
- Admin -> Configuración -> Editar la configuración básica -> Configuración de características: Activar permitir subidas.

## Referencias

- https://hub.docker.com/r/linuxserver/calibre
- https://www.cduser.com/como-tener-tu-libreria-digital-con-calibre-web/