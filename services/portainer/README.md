# Portainer 

Servicio encargado de gestionar todos los contenedores instalados.

## Instalación
1. En la terminal, ejecutar:
```
make portainer
```
2. Una vez instalado, acceder a: http://ipsmarthome:9000