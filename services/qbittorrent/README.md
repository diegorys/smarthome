# qBitTorrent

Servicio encargado de la descarga de archivos torrent.

## Instalación

En el archivo [docker-compose.yml](./docker-compose.yml) mapeamos el directorio de descargas a la rutas correspondiente de tu disco duro externo.

1. En la terminal, ejecutar la orden:
```
make qbittorrent
```
2. En tu navegador, accede a: http://ipsmarthome:8090.
- NOTA: Si accedes desde Heimdall, te aparecerá un error indicando: **Unauthorized**. Debes escribir manualmente la dirección en el navegador.