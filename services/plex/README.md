# Plex

Servicio encargado de organizar tu colección de películas, series, música y fotos.

## Instalación

En el archivo [docker-compose.yml](./docker-compose.yml) mapeamos los directorios a las rutas correspondientes de tu disco duro externo.

1. En la terminal, ejecutar la orden:
```
make plex
```
2. En tu navegador, accede a: http://ipsmarthome:32400.