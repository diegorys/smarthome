# Heimdall

Servicio encargado de mostrar una puerta de entrada al servidor, con un listado a modo de mosaicos de todos los servicios instalados y su enlace para acceder a ellos.

## Instalación

1. En la terminal, ejecutar la orden:
```
make heimdall
```
2. En tu navegador, accede a: http://ipsmarthome. Según se instale cada servicio, se debe insertar un enlace al mismo.